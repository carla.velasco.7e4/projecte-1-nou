<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="../guia.css">
    <title>Tenda</title>
</head>

<body id="body" class="body">
    <header class="header" id="header">
        <nav class="nav container">
            <div class="nav__logo">
                <img src="../img/logo_transparent.png" class="iconlogo">
            </div>


            <div class="nav__menu" id="nav-menu">
                <ul class="nav__list" id="nav-list">
                    <li class="nav__item">
                        <a href="../index.php#productesDestacats" class="nav__link nav--color">Productes destacats</a>
                    </li>
                    <li class="nav__item">
                        <a href="../index.php#productes" class="nav__link nav--color">Tots els productes</a>
                    </li>
                    <li class="nav__item">
                        <a href="../index.php#noticies1" class="nav__link nav--color">Noticies</a>
                    </li>
                    <li class="nav__item">
                        <a href="../index.php#nosltres" class="nav__link nav--color">Sobre Nostaltres</a>
                    </li>
                    <li class="nav__item">
                        <a href="../index.php#contacta" class="nav__link nav--color">Contacte</a>
                    </li>
                    <li>
                      <a style="font-size:30px;cursor:pointer" onclick="openNav()">Cistella &#128722;</a>
                    </li>
                    <li>
                      <div id="mySidenav" class="sidenav">
                      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <hr>
                        <?php 
                         include_once('../php/model.php');
                         include_once('../php/test.php');

                        foreach ($catalegObjecte->productes as $producte) {
                            echo"<div class='productes'>";
                            echo"   <div class='producte'>";
                            echo"     <ul class='llista_productes'>";
                            echo"        <li class='producte'>";
                            echo"            <div class='mini_img'><img src='" . $producte->fotos[0] ."' alt='$producte->titol' width='35%'></div>";  
                            echo"            <div class='producte_caracteristiques'>";
                            echo"               <p class='nom_producte'>". $producte->titol . "</p>";
                            echo"               <p class='color_producte'>". $producte->colors[0] . "</p>";
                            echo"               <p class='color_producte'>". $producte->colors[1] . "</p>";
                            echo"               <p class='color_producte'>". $producte->colors[2] . "</p>";
                            echo"               <p class='color_producte'>". $producte->colors[3] . "</p>";
                            echo"               <p class='color_producte'>". $producte->colors[4] . "</p>";
                            echo"               <p class='quantitat_producte'>Unitats: ". $producte->quantitat . "</p>";
                            echo"               <p class='preu_producte'>Preu: ". $producte->preu . "</p>";
                            echo"               <input type='hidden' name='id' value='".$producte->id."'>";
                            echo"            </div>";
                            echo"        </li>";
                            echo"     </ul>";
                            echo"   </div>";
                            echo"</div>";
                        }
                        ?>
                      </div>
                    </li>
                </ul>
            </div>

            <div class="nav__toggle" id="nav-toggle">
                <div class="nav__t-line nav__t-line--line1"></div>
                <div class="nav__t-line nav__t-line--line2"></div>
                <div class="nav__t-line nav__t-line--line3"></div>
            </div>

        </nav>
    </header>
                    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <footer>
        <p id="contacta"></p>
        <br>
        <div class="flex-around foot75">
            <div class="foot75 flex-start">
                <div class="flex-column foot75">
                    <br>
                    <br>
                    <p>Puedes encontrarnos en (direccion) o bien puedes llamarnos al (telefono)</p>
                    <br>
                    <p>Carrer d'Aiguablava, 121, 08033 Barcelona</p>
                    <p>Telefon: <a href="tel:+34937070020">937 07 00 20</a></p>
                    <p>E-Mail: <a
                            href="https://mail.google.com/mail/?view=cm&fs=1&to=rafael.cuestas@itb.cat">latiendadejuan@itb.cat</a>
                    </p>
                    <br>
                    <p class="bold">SÍGUENOS EN NUESTRAS REDES SOCIALES</p>
                    <div class="flex text-center marginTop1em flex-center">
                        <div class="section-media flex width20 justify-center">
                            <div class="icon icon-facebook ">
                                <a href="https://ca-es.facebook.com/"><span class="iconify"
                                        data-icon="entypo-social:facebook-with-circle"></span></a>
                            </div>
                            <div class="icon icon-instagram">
                                <a href="https://www.instagram.com/"><span class="iconify"
                                        data-icon="entypo-social:instagram-with-circle"></span></a>
                            </div>
                            <div class="icon icon-youtube ">
                                <a href="https://www.youtube.com/?hl=CA"><span class="iconify"
                                        data-icon="entypo-social:youtube-with-circle"></span></a>
                            </div>
                            <div class="icon icon-twitter ">
                                <a href="https://twitter.com/?lang=ca"><span class="iconify"
                                        data-icon="entypo-social:twitter-with-circle"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Seccion del mapa-->
            <div class="mapa" id="mapa">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.3551869645594!2d2.1843419159820963!3d41.45321107925814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bcfdad2100a9%3A0x3c4e7db0a61a83e0!2sInstitut%20Tecnol%C3%B2gic%20de%20Barcelona!5e0!3m2!1sca!2ses!4v1634652269127!5m2!1sca!2ses"
                    width="500" height="375" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </footer>

    <script src="../js/array.js"></script>
    <script src="../js/script.js"></script>
    <script src="https://code.iconify.design/2/2.0.4/iconify.min.js"></script>
</body>

</html>