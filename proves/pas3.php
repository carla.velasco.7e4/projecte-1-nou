<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    //BLOC INICI SESSIO - POSAR AL PRINCIPI DE CADA PAGINA
    session_start();
    include_once('../php/model.php');
    include_once('../php/test.php');

    
    if(!isset($_SESSION['cataleg']) && !isset($_SESSION['cistella'])) {
        $cistellaSerialitzada = serialize($cistellaObjecte);
        $_SESSION['cistella']= $cistellaSerialitzada;
        
        $catalegSerialitzat = serialize($catalegObjecte);
        $_SESSION['cataleg']= $catalegSerialitzat;
    } 
    
    
    $catalegSerialitzat = $_SESSION['cataleg'];
    $catalegObjecte = unserialize($catalegSerialitzat);
    
    $cistellaSerialitzada = $_SESSION['cistella'];
    $cistellaObjecte = unserialize($cistellaSerialitzada);
    //FI BLOC INICI SESSIO




    $lId = $_REQUEST['id'];

    $cistellaObjecte->addProducte($catalegObjecte->getProducteById($_REQUEST['id']));




    //BLOC FI SESSIO - POSAR AL FINAL DE CADA PAGIA
    $cistellaSerialitzada = serialize($cistellaObjecte);
    $_SESSION['cistella']= $cistellaSerialitzada;

    $catalegSerialitzat = serialize($catalegObjecte);
    $_SESSION['cataleg']= $catalegSerialitzat;
    // FI BLOC FINAL SESSIO

    
    //RETORN AUTOMATIC A LA PAGINA DEL FORMULARI
    echo "<script>location.href='../proves/pas2.php'</script>";
    ?>
</body>
</html>