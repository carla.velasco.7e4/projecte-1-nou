<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>

<body>
    <header class="header" id="header">
        <nav class="nav container">
            <div class="nav__logo-wrap">


                <div class="nav__toggle" id="">
                    <div class="nav__toggle-item nav__toggle-lin1"></div>
                    <div class="nav__toggle-item nav__toggle-lin2"></div>
                    <div class="nav__toggle-item nav__toggle-lin3"></div>
                </div>


                <div class="nav__logo">
                    <a href="#" class="nav__link-logo">
                        LOGO
                    </a>
                </div>


                <a href="#usuari" class="nav__link nav__productes">
                    <div class="nav__toggle" id="nav-toggle">
                        <div class="nav__toggle-item nav__toggle-lin1"></div>
                        <div class="nav__toggle-item nav__toggle-lin2"></div>
                        <div class="nav__toggle-item nav__toggle-lin3"></div>
                    </div>
                    <span class="nav__title">
                        Tots els productes
                    </span>
                </a>
            </div>




            <fieldset class="nav__searchbox searchbox">
                <input type="text" placeholder="Cercar producte..." class="searchbox__input" />
                <div class="searchbox__icons-container">
                    <div class="searchbox__icon-search"></div>
                    <div class="searchbox__icon-close">
                        <div class="x-up"></div>
                        <div class="x-down"></div>
                    </div>
                </div>
            </fieldset>




            <div class="nav__menu" id="nav-menu">
                <ul class="nav__list" id="nav-list">
                    <li class="nav__item">
                        <a href="#usuari" class="nav__link">
                            <svg class="nav__svg" style="margin:0 8px 0 0;width:24px;height:24px"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"
                                class="Svg-sc-1w4q54b iyEeRT">
                                <path
                                    d="M12 12c-2.2 0-4-1.8-4-4s1.8-4 4-4 4 1.8 4 4-1.8 4-4 4zm0-6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zM20 20h-16v-1c0-3.5 3.3-6 8-6s8 2.5 8 6v1zm-13.8-2h11.7c-.6-1.8-2.8-3-5.8-3s-5.3 1.2-5.9 3z"
                                    fill="#fff">
                                </path>
                            </svg>
                            <span class="nav__title">
                                Usuari
                            </span>
                        </a>
                    </li>


                    <li class="nav__item">
                        <a href="#cistell" class="nav__link" onclick="openNav()">
                            <svg class="nav__svg" style="margin:0 8px 0 0;width:24px;height:24px" viewBox="0 0 24 24"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6 2C6.5 2 6.9 2.3 7 2.8L8 7L21 7C21.3 7 21.6 7.1 21.8 7.4C22 7.6 22 7.9 22 8.2L20 16.2C19.9 16.7 19.5 17 19 17L9 17C8.5 17 8.1 16.7 8 16.2L5.2 4L2 4V2L6 2ZM9.8 15L18.2 15L19.7 9L8.4 9L9.8 15ZM18 22C16.8954 22 16 21.1046 16 20C16 18.8954 16.8954 18 18 18C19.1046 18 20 18.8954 20 20C20 21.1046 19.1046 22 18 22ZM8 20C8 21.1046 8.89543 22 10 22C11.1046 22 12 21.1046 12 20C12 18.8954 11.1046 18 10 18C8.89543 18 8 18.8954 8 20Z"
                                    fill="#fff"></path>
                            </svg>
                            <span class="nav__title">
                                Cistell
                            </span>
                            <div id="idCart" style="display:none">ANON-a4d42361-39ce-4da8-bb61-c22949b84239</div>
                            <span class="nav__units c-units js-units" style="margin:0 0 0 8px;">5</span>
                        </a>
                    </li>
                </ul>
        </nav>
        <nav class="nav-secundaria">
            <div class="nav-secundaria__menu" id="nav-secundaria-menu">
                <ul class="nav-secundaria__list" id="nav-secundaria-list">

                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" style="margin:0 8px 0 0; padding: 1   px 0" width="18pt" height="18pt" viewBox="0 0 513.000000 512.000000" preserveAspectRatio="xMidYMid meet">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M157 4805 c-58 -22 -94 -52 -124 -103 l-28 -47 -3 -1913 c-2 -1623 0 -1923 12 -1981 23 -109 71 -195 156 -282 77 -78 140 -117 250 -155 53 -18 122 -19 2140 -19 2018 0 2087 1 2140 19 110 38 173 77 250 155 52 53 85 97 108 145 66 135 63 42 60 1829 l-3 1624 -30 49 c-19 30 -49 60 -79 79 l-49 30 -208 3 -208 3 -3 208 -3 208 -30 49 c-19 30 -49 60 -79 79 l-49 30 -2091 2 c-1769 1 -2097 0 -2129 -12z m4155 -2022 l3 -1808 27 -51 c40 -77 110 -132 187 -149 112 -24 181 16 181 106 0 61 -34 99 -105 114 -16 4 -38 14 -48 22 -16 15 -17 100 -17 1505 l0 1488 175 0 175 0 0 -1569 c0 -1005 -4 -1587 -10 -1621 -22 -115 -90 -202 -200 -254 l-65 -31 -2055 0 -2055 0 -65 31 c-110 52 -178 139 -200 254 -6 34 -10 713 -10 1911 l0 1859 2040 0 2040 0 2 -1807z"/>
                                <path d="M734 4086 c-29 -29 -34 -41 -34 -79 0 -37 6 -51 29 -75 17 -16 44 -32 62 -36 56 -11 2930 -7 2971 4 48 14 81 58 80 110 -1 45 -16 72 -53 94 -25 15 -163 16 -1524 16 l-1497 0 -34 -34z"/>
                                <path d="M735 3507 c-26 -26 -34 -44 -40 -88 -3 -29 -5 -310 -3 -624 3 -621 1 -592 60 -633 21 -15 83 -17 621 -20 397 -2 611 1 637 8 25 7 47 22 62 43 l23 32 3 585 c2 322 0 604 -3 627 -4 29 -16 53 -36 73 l-31 30 -630 0 -630 0 -33 -33z m1125 -667 l0 -470 -465 0 -465 0 0 470 0 470 465 0 465 0 0 -470z"/>
                                <path d="M2482 3510 c-29 -29 -40 -64 -34 -107 5 -32 55 -79 93 -87 41 -8 1167 -8 1208 0 38 8 88 55 93 87 6 43 -5 78 -34 107 l-30 30 -633 0 -633 0 -30 -30z"/>
                                <path d="M2538 2963 c-15 -2 -41 -18 -58 -34 -25 -26 -30 -38 -30 -79 0 -40 5 -53 29 -78 17 -16 44 -32 62 -36 41 -8 1167 -8 1208 0 18 4 45 20 62 36 24 25 29 38 29 78 0 40 -5 53 -29 78 -18 17 -45 32 -68 35 -41 7 -1160 7 -1205 0z"/>
                                <path d="M2533 2383 c-13 -2 -36 -18 -53 -34 -25 -26 -30 -38 -30 -79 0 -40 5 -53 29 -78 18 -17 45 -32 68 -35 50 -9 1146 -9 1196 0 23 3 50 18 68 35 24 25 29 38 29 78 0 40 -5 53 -29 78 -17 16 -44 32 -62 36 -34 7 -1180 6 -1216 -1z"/>
                                <path d="M783 1803 c-13 -2 -36 -18 -53 -34 -24 -25 -30 -39 -30 -76 0 -38 5 -50 34 -79 l34 -34 630 0 630 0 31 31 c61 61 35 166 -47 189 -36 10 -1179 13 -1229 3z"/>
                                <path d="M2533 1803 c-30 -6 -81 -57 -85 -86 -6 -43 5 -78 34 -107 l30 -30 633 0 633 0 30 30 c29 29 40 64 34 107 -5 32 -55 79 -93 87 -34 7 -1180 6 -1216 -1z"/>
                                <path d="M783 1223 c-13 -2 -36 -18 -53 -34 -24 -25 -30 -39 -30 -76 0 -38 5 -50 34 -79 l34 -34 621 0 c675 0 661 -1 691 55 33 61 4 139 -61 162 -25 9 -191 12 -624 11 -324 0 -600 -3 -612 -5z"/>
                                <path d="M2523 1219 c-62 -18 -97 -111 -62 -167 33 -53 18 -52 684 -52 666 0 651 -1 684 52 36 58 0 149 -67 168 -47 13 -1194 12 -1239 -1z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Noticies
                            </span>
                        </a>
                    </li>

                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link active">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="16.000000pt" height="16.000000pt" viewBox="0 0 512.000000 512.000000" style="margin:0 8px 0 0;" preserveAspectRatio="xMidYMid meet">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M3383 5090 c-122 -17 -219 -36 -246 -48 -205 -91 -523 -403 -730 -719 -317 -484 -476 -1021 -481 -1625 0 -115 -2 -208 -3 -208 -12 0 -97 136 -140 225 -115 240 -147 488 -104 804 12 85 21 157 21 161 0 4 -50 20 -111 35 -91 23 -113 25 -119 14 -5 -7 -23 -54 -41 -104 -98 -276 -191 -489 -359 -826 -94 -186 -157 -332 -199 -459 -127 -377 -117 -796 26 -1180 203 -545 673 -965 1233 -1104 157 -39 256 -51 435 -50 181 0 272 11 427 50 319 80 591 235 829 475 338 339 519 782 519 1270 0 323 -62 607 -190 877 -119 249 -229 405 -560 792 -224 263 -293 363 -359 517 -95 221 -96 452 -3 654 34 74 90 151 192 264 159 176 191 215 179 214 -8 -1 -105 -14 -216 -29z m-495 -1021 c28 -117 60 -197 122 -309 92 -166 133 -221 500 -655 354 -421 501 -748 529 -1179 20 -307 -34 -575 -164 -824 -74 -142 -152 -248 -270 -366 -280 -282 -649 -436 -1043 -436 -518 0 -985 263 -1261 708 -139 224 -208 456 -218 732 -8 220 24 405 106 610 47 116 225 480 231 473 4 -4 17 -39 29 -77 62 -195 178 -385 326 -531 115 -114 180 -155 383 -239 112 -47 132 -52 132 -37 0 9 -11 92 -24 183 -51 345 -58 643 -21 923 64 485 261 952 549 1304 l61 74 6 -134 c4 -78 15 -170 27 -220z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Més venuts
                            </span>
                        </a>
                    </li>

                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="16.000000pt" height="16.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet" style="margin:0 8px 0 0;">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M2440 5105 c-139 -39 -191 -80 -361 -291 -133 -164 -151 -181 -216 -199 -14 -4 -105 16 -231 49 -171 46 -222 56 -289 56 -102 0 -190 -26 -272 -81 -78 -52 -125 -107 -167 -194 -40 -84 -51 -148 -59 -360 -11 -271 -8 -267 -252 -360 -98 -37 -205 -81 -238 -99 -170 -89 -276 -298 -248 -491 13 -95 42 -155 161 -338 123 -188 122 -187 122 -237 0 -50 1 -49 -122 -237 -119 -183 -148 -243 -161 -338 -28 -193 78 -402 248 -491 33 -18 140 -62 238 -99 244 -93 241 -89 252 -360 9 -215 19 -277 60 -360 43 -89 89 -143 166 -194 82 -55 170 -81 272 -81 67 0 118 10 289 55 182 49 212 55 245 46 21 -6 46 -16 56 -23 9 -7 79 -89 154 -182 151 -185 203 -230 313 -268 91 -32 229 -32 320 0 110 38 162 83 313 268 75 93 145 175 154 182 10 7 35 17 56 23 33 9 63 3 245 -46 171 -45 222 -55 289 -55 102 0 190 26 272 81 77 51 123 105 166 194 41 83 51 145 60 360 11 271 8 267 264 365 101 38 206 82 234 97 163 89 267 300 240 488 -13 95 -42 155 -161 338 -123 188 -122 187 -122 237 0 50 -1 49 122 237 119 183 148 243 161 338 27 188 -77 399 -240 488 -28 15 -133 59 -234 97 -256 98 -253 94 -264 365 -9 215 -19 277 -60 360 -43 89 -89 143 -166 194 -82 55 -170 81 -272 81 -67 0 -118 -10 -289 -56 -126 -33 -217 -53 -231 -49 -65 18 -83 35 -216 200 -152 188 -205 234 -310 273 -84 31 -212 38 -291 17z m200 -344 c24 -17 47 -43 230 -265 129 -156 236 -211 410 -211 83 1 119 7 286 53 105 29 204 52 220 52 43 0 105 -40 125 -80 15 -28 21 -77 29 -235 6 -110 15 -223 21 -251 23 -118 108 -241 214 -309 28 -18 136 -65 240 -105 105 -40 201 -82 213 -94 40 -36 55 -74 50 -130 -4 -45 -19 -73 -121 -229 -136 -209 -161 -270 -161 -397 0 -127 25 -188 161 -397 102 -156 117 -184 121 -229 5 -56 -10 -94 -50 -130 -12 -12 -108 -54 -213 -94 -104 -40 -212 -87 -240 -105 -106 -68 -191 -191 -214 -309 -6 -28 -15 -141 -21 -251 -8 -158 -14 -207 -29 -235 -21 -40 -82 -80 -126 -80 -16 0 -115 23 -220 52 -166 46 -202 52 -285 53 -174 0 -281 -55 -410 -211 -246 -298 -234 -286 -310 -286 -76 0 -64 -12 -310 286 -129 156 -236 211 -410 211 -83 -1 -119 -7 -285 -53 -105 -29 -204 -52 -220 -52 -44 0 -105 40 -126 80 -15 28 -21 77 -29 235 -6 110 -15 223 -21 251 -23 118 -108 241 -214 309 -27 18 -135 65 -240 105 -104 40 -201 82 -213 94 -40 36 -55 74 -50 130 4 45 19 73 121 229 136 209 161 270 161 397 0 127 -25 188 -161 397 -102 156 -117 184 -121 229 -5 56 10 94 50 130 12 12 97 50 188 84 217 83 272 111 342 178 66 62 120 160 137 244 5 29 15 143 21 253 8 158 14 207 29 235 21 40 82 80 126 80 17 0 114 -23 215 -50 221 -61 297 -69 402 -46 74 16 167 63 217 109 13 12 79 90 146 172 68 83 134 160 148 173 47 44 125 50 177 13z"/>
                                <path d="M1788 3447 c-100 -38 -158 -89 -206 -181 -22 -43 -26 -64 -27 -136 0 -72 4 -94 26 -141 123 -261 477 -260 607 1 23 46 27 68 27 135 -1 135 -64 239 -181 296 -76 38 -184 49 -246 26z"/>
                                <path d="M3200 3441 c-19 -11 -393 -379 -831 -818 -852 -854 -828 -826 -816 -915 7 -49 49 -106 93 -124 40 -17 107 -18 137 -3 31 16 1625 1608 1643 1641 35 67 9 161 -58 211 -39 29 -123 33 -168 8z"/>
                                <path d="M2982 2208 c-74 -28 -153 -108 -182 -185 -30 -79 -26 -196 10 -268 30 -61 94 -125 157 -156 40 -20 63 -24 143 -24 84 0 101 3 147 28 66 34 116 84 150 150 25 46 28 63 28 147 0 114 -22 168 -97 242 -88 86 -234 113 -356 66z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Promocions
                            </span>

                        </a>
                    </li>

                    
                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="14.000000pt" height="14.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet" style="margin:0 8px 0 0;">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M2321 5110 c-497 -48 -990 -251 -1376 -565 -114 -92 -294 -274 -384 -387 -229 -287 -417 -675 -495 -1023 -49 -218 -60 -325 -60 -575 0 -250 11 -357 60 -575 79 -355 272 -749 509 -1040 92 -114 274 -294 387 -384 287 -229 675 -417 1023 -495 218 -49 325 -60 575 -60 250 0 357 11 575 60 261 58 603 204 828 353 389 259 688 599 893 1016 125 255 196 484 241 775 24 161 24 539 0 700 -45 291 -116 520 -241 775 -134 272 -283 480 -498 692 -211 209 -404 346 -673 478 -252 124 -486 197 -765 240 -126 19 -468 27 -599 15z m559 -246 c508 -74 967 -303 1324 -660 359 -358 581 -804 662 -1329 12 -77 17 -172 17 -315 0 -378 -75 -698 -240 -1032 -343 -693 -980 -1152 -1768 -1274 -151 -23 -479 -23 -630 0 -525 81 -971 303 -1329 662 -359 358 -581 804 -662 1329 -23 151 -23 479 0 630 42 267 111 492 223 717 337 680 970 1144 1733 1268 193 32 468 33 670 4z"/>
                                <path d="M2711 3826 c-94 -31 -184 -116 -211 -200 -31 -98 5 -225 85 -295 176 -154 471 -88 541 121 62 188 -81 376 -295 385 -47 2 -93 -3 -120 -11z"/>
                                <path d="M2381 2969 c-69 -12 -342 -105 -357 -122 -11 -12 -47 -137 -41 -143 1 -2 32 6 67 17 140 44 270 24 304 -47 35 -75 18 -180 -94 -561 -86 -294 -101 -364 -102 -478 -1 -74 3 -99 21 -142 30 -67 103 -136 189 -176 60 -28 78 -31 183 -35 141 -5 207 8 394 76 l130 48 19 67 c11 37 17 69 14 72 -3 3 -25 -2 -49 -11 -65 -25 -233 -25 -277 -1 -104 57 -98 153 42 634 88 301 96 338 97 455 1 167 -53 253 -201 321 -58 27 -77 31 -175 33 -60 2 -135 -1 -164 -7z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Sobre nosaltres
                            </span>
                        </a>
                    </li>

                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="14.000000pt" height="14.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet" style="margin:0 8px 0 0;">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M2321 5110 c-497 -48 -990 -251 -1376 -565 -114 -92 -294 -274 -384 -387 -229 -287 -417 -675 -495 -1023 -49 -218 -60 -325 -60 -575 0 -250 11 -357 60 -575 79 -355 272 -749 509 -1040 92 -114 274 -294 387 -384 287 -229 675 -417 1023 -495 218 -49 325 -60 575 -60 250 0 357 11 575 60 261 58 603 204 828 353 389 259 688 599 893 1016 125 255 196 484 241 775 24 161 24 539 0 700 -45 291 -116 520 -241 775 -134 272 -283 480 -498 692 -211 209 -404 346 -673 478 -252 124 -486 197 -765 240 -126 19 -468 27 -599 15z m559 -246 c508 -74 967 -303 1324 -660 359 -358 581 -804 662 -1329 12 -77 17 -172 17 -315 0 -378 -75 -698 -240 -1032 -343 -693 -980 -1152 -1768 -1274 -151 -23 -479 -23 -630 0 -525 81 -971 303 -1329 662 -359 358 -581 804 -662 1329 -23 151 -23 479 0 630 42 267 111 492 223 717 337 680 970 1144 1733 1268 193 32 468 33 670 4z"/>
                                <path d="M2711 3826 c-94 -31 -184 -116 -211 -200 -31 -98 5 -225 85 -295 176 -154 471 -88 541 121 62 188 -81 376 -295 385 -47 2 -93 -3 -120 -11z"/>
                                <path d="M2381 2969 c-69 -12 -342 -105 -357 -122 -11 -12 -47 -137 -41 -143 1 -2 32 6 67 17 140 44 270 24 304 -47 35 -75 18 -180 -94 -561 -86 -294 -101 -364 -102 -478 -1 -74 3 -99 21 -142 30 -67 103 -136 189 -176 60 -28 78 -31 183 -35 141 -5 207 8 394 76 l130 48 19 67 c11 37 17 69 14 72 -3 3 -25 -2 -49 -11 -65 -25 -233 -25 -277 -1 -104 57 -98 153 42 634 88 301 96 338 97 455 1 167 -53 253 -201 321 -58 27 -77 31 -175 33 -60 2 -135 -1 -164 -7z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Ultimes unitats
                            </span>
                        </a>
                    </li>

                    <li class="nav-secundaria__item">
                        <a href="#" class="nav-secundaria__link">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="18.000000pt" height="18.000000pt" viewBox="0 0 512.000000 512.000000" style="margin:0 8px 0 0;" preserveAspectRatio="xMidYMid meet">
                                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                                <path d="M1143 4651 c-55 -14 -230 -119 -316 -190 -231 -191 -398 -469 -462 -772 -38 -175 -35 -451 5 -648 41 -202 99 -369 195 -561 75 -150 585 -1027 663 -1139 337 -485 797 -780 1263 -809 295 -19 554 40 789 180 87 52 110 71 135 112 29 48 30 54 29 160 0 96 -4 121 -28 186 -41 108 -93 207 -186 355 -139 220 -217 311 -330 384 -87 57 -153 74 -254 69 -93 -5 -141 -24 -306 -121 -100 -59 -108 -62 -170 -62 -81 0 -157 35 -223 104 -29 30 -125 185 -284 461 -132 228 -249 431 -259 450 -26 51 -32 197 -9 255 22 58 51 84 171 155 l98 58 17 -27 c26 -43 1073 -861 1153 -901 64 -32 77 -35 155 -34 70 0 96 5 141 26 35 16 324 236 780 594 399 314 731 573 738 578 9 6 12 -183 12 -929 l0 -936 -25 -24 -24 -25 -518 0 c-333 0 -532 -4 -556 -11 -75 -21 -101 -112 -48 -171 l29 -33 564 0 c556 0 564 0 610 22 66 30 132 95 157 153 l21 50 -2 1057 -3 1058 -27 47 c-34 56 -92 104 -158 129 -48 18 -101 19 -1453 19 l-1403 0 -88 173 c-108 211 -178 318 -275 420 -107 112 -222 162 -318 138z m113 -239 c79 -57 165 -179 277 -392 88 -168 128 -285 130 -375 2 -117 -8 -127 -242 -261 -150 -86 -212 -164 -247 -311 -20 -85 -14 -225 14 -303 18 -50 487 -870 553 -967 63 -91 190 -178 304 -208 77 -20 192 -19 252 1 26 9 107 50 178 92 153 88 202 99 275 62 94 -48 196 -168 328 -387 91 -151 140 -261 156 -352 12 -74 13 -74 -108 -140 -286 -158 -674 -173 -987 -37 -282 121 -548 353 -748 650 -65 97 -577 979 -633 1091 -238 475 -272 969 -93 1340 58 120 114 196 229 310 82 82 134 124 201 162 50 29 97 53 106 53 9 0 34 -13 55 -28z m3278 -711 c-13 -12 -1366 -1074 -1432 -1124 -63 -48 -129 -60 -180 -33 -17 9 -266 200 -552 425 -286 225 -529 415 -540 422 -19 14 -19 15 11 76 26 51 32 78 37 153 l5 90 1330 0 c776 0 1327 -4 1321 -9z"/>
                                </g>
                                </svg>
                            <span class="nav-secundaria__title">
                                Contacte
                            </span>
                        </a>
                    </li>

                </ul>
        </nav>
    </header>

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="openNav()">&times;</a>
        <hr>
        <?php 
        include_once('./php/model.php');
        include_once('./php/test.php');

        //include_once('./carpeta/test.php');
        
        if(!isset($_SESSION['cataleg']) && !isset($_SESSION['cistella'])) {
            $cistellaSerialitzada = serialize($cistellaObjecte);
            $_SESSION['cistella']= $cistellaSerialitzada;
            
            $catalegSerialitzat = serialize($catalegObjecte);
            $_SESSION['cataleg']= $catalegSerialitzat;
        }  
        
    
        $catalegSerialitzat = $_SESSION['cataleg'];
        $catalegObjecte = unserialize($catalegSerialitzat);
        
        $cistellaSerialitzada = $_SESSION['cistella'];
        $cistellaObjecte = unserialize($cistellaSerialitzada);
        //FI BLOC INICI SESSIO

        foreach ($catalegObjecte->productes as $producte) {
            echo"<div class='productes'>";
            echo"   <div class='producte'>";
            echo"     <ul class='llista_productes'>";
            echo"        <li class='producte'>";
            echo"            <div class='mini_img'><img src='" . $producte->fotos[0] ."' alt='$producte->titol' width='35%'></div>";  
            echo"            <div class='producte_caracteristiques'>";
            echo"               <p class='nom_producte'>". $producte->titol . "</p>";
            echo"               <p class='color_producte'>". $producte->colors[0] . "</p>";
            echo"               <p class='color_producte'>". $producte->colors[1] . "</p>";
            echo"               <p class='color_producte'>". $producte->colors[2] . "</p>";
            echo"               <p class='color_producte'>". $producte->colors[3] . "</p>";
            echo"               <p class='color_producte'>". $producte->colors[4] . "</p>";
            echo"               <p class='quantitat_producte'>Unitats: ". $producte->quantitat . "</p>";
            echo"               <p class='preu_producte'>Preu: ". $producte->preu . "</p>";
            echo"               <input type='hidden' name='id' value='".$producte->id."'>";
            echo"            </div>";
            echo"        </li>";
            echo"     </ul>";
            echo"   </div>";
            echo"</div>";
        }
        
        echo "</div>";
                        
        echo"<main class='productes1' id='productes1'>";
        echo"    <section class='section'>";
        echo"        <article class='products'>";
            
                
        //BLOC INICI SESSIO - POSAR AL PRINCIPI DE CADA PAGINA
        //session_unset();
        
        foreach ($catalegObjecte->productes as $producte) {
            echo"<div class='productes'>";
            echo"<ul class='llista-productes'>";
            echo"    <form action='producte.php' method='get'>";
            echo"        <li class='producte'>";
            echo"            <div class='mini-imatge'><img class='width100' src='" . $producte->fotos[0] ."' alt='' srcset='' height='100px'></div>";
            echo"            <div class='nom-producte'>". $producte->id . "</div>";
            echo"            <input type='hidden' name='id' value='".$producte->id."'>";
            echo"            <input class='submit' type='submit' value='+'>";
            echo"        </li>";
            echo"    </form>";
            echo"</ul>";
            echo"</div>";
        }

        
        //BLOC FI SESSIO - POSAR AL FINAL DE CADA PAGIA
        $cistellaSerialitzada = serialize($cistellaObjecte);
        $_SESSION['cistella']= $cistellaSerialitzada;

        $catalegSerialitzat = serialize($catalegObjecte);
        $_SESSION['cataleg']= $catalegSerialitzat;
        // FI BLOC FINAL SESSIO

        ?>

            </article>
        </section>
    </main>

    <footer>
   <div class="content">
     <div class="left box">
       <div class="upper">
         <div class="topic">About us</div>
         <p>CodingLab is a channel where you can learn HTML,
         CSS, and also JavaScript along with creative CSS Animations and Effects.</p>
       </div>
       <div class="lower">
         <div class="topic">Contact us</div>
         <div class="phone">
           <a href="#"><i class="fas fa-phone-volume"></i>+007 9089 6767</a>
         </div>
         <div class="email">
           <a href="#"><i class="fas fa-envelope"></i>abc@gmail.com</a>
         </div>
       </div>
     </div>
     <div class="middle box">
       <div class="topic">Our Services</div>
       <div><a href="#">Web Design, Development</a></div>
       <div><a href="#">Web UX Design, Reasearch</a></div>
       <div><a href="#">Web User Interface Design</a></div>
       <div><a href="#">Theme Development, Design</a></div>
       <div><a href="#">Mobile Application Design</a></div>
       <div><a href="#">Wire raming & Prototyping</a></div>
     </div>
     <div class="right box">
       <div class="topic">Subscribe us</div>
       <form action="#">
         <input type="text" placeholder="Enter email address">
         <input type="submit" name="" value="Send">
         <div class="media-icons">
           <a href="#"><i class="fab fa-facebook-f"></i></a>
           <a href="#"><i class="fab fa-instagram"></i></a>
           <a href="#"><i class="fab fa-twitter"></i></a>
           <a href="#"><i class="fab fa-youtube"></i></a>
           <a href="#"><i class="fab fa-linkedin-in"></i></a>
         </div>
       </form>
     </div>
   </div>
   <div class="bottom">
     <p>Copyright © 2020 <a href="#">CodingLab</a> All rights reserved</p>
   </div>
 </footer>

 <script src="./js/script.js"></script>
</body>
</html>