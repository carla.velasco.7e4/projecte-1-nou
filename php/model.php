<?php

class Producte
{
    public $id;
    static $contador = -1;
    public $titol;
    public $categoria;
    public $preu;
    public $descripció;
    public $colors;
    public $stoc;
    public $caracteristiques;
    public $especificacions;
    public $instruccions;
    static $prova;
    public $fotos = [];
    public $quantitat = 0;

    function Producte()
    {
        self::$contador++;
        $this->id = self::$contador;
    }

    function getId()
    {
        echo "$this->id";
    }

    function addFoto($lFoto)
    {
        array_push($this->fotos, $lFoto);
    }
}


class Cataleg
{
    public $productes = [];

    function addProducte($lProducte)
    {
        $this->productes[] = $lProducte;
    }

    function getProducteById($lId)
    {
        foreach ($this->productes as $producte) {
            if ($producte->id == $lId) {
                return $producte;
            }
        }
    }

    function llistar()
    {
        foreach ($this->productes as $producte) {
            var_dump($producte);
        }
    }
}


class Cistell
{
    public $productes = [];

    function addProducte($lProducte)
    {
        $this->productes[] = $lProducte;
        foreach ($this->productes as $producte) {
            if ($producte->id == $lProducte->id) {
                $lProducte->quantitat++;
                break;
            }
        }
    }

    function deleteProducte($lId)
    {
        foreach ($this->productes as $producte) {
            $this->productes = [];
            if ($producte->id == $lId) {
                $producte->quantitat--;
                continue;
            }
            $this->productes[] = $producte;
        }
    }

    function llistar()
    {
        foreach ($this->productes as $producte) {
            var_dump($producte);
        }
    }

    function getTotal()
    {
        $preuTotal = 0;
        foreach ($this->productes as $producte) {
            $preuTotal += $producte->preu;
        }
        return $preuTotal;
    }

    function getNumProductes()
    {
        $numProductes = 0;
        foreach ($this->productes as $producte) {
            $numProductes++;
        }
        return $numProductes;
    }

    function getProducteById($lId)
    {
        foreach ($this->productes as $producte) {
            if ($producte->id == $lId) {
                return $producte;
            }
        }
    }

    function buidar()
    {
        foreach ($this->productes as $producte) {
            $producte->quantitat = 0;
        }
        unset($this->productes);
    }
}

?>
