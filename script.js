var prod1 = [
  {
      img: "./img/pc.jpg",
      title: "NOX HUMMER MC USB 3.0 NEGRA",
      descripcion: "Il·luminació LED de 7 colors, HD Audio + USB 2.0-3.0, 2 sistemes de ventilació inclosos, Xassís d'acer SPCC 0.5 mm i Panell frontal de plàstic ABS",
      precio: "59,49 €"
  },
  {
      img: "./img/nintendoswitch.jpg",
      title: "NINTENDO SWITCH – MODELO OLED 7",
      descripcion: "Pantalla HD - (1.280 x 720 px), 4 GB de RAM + 64GB, Pes - 320 g, Bateria - 5,5 h",
      precio: "349,99 €"
  },
  {
      img: "./img/televisor.jpg",
      title: "XIAOMI MI TV 4S 55",
      descripcion: "Smart TV 4K+HDR, 2 GB de RAM + 8 GB, Bluetooth 4.2, Wi-Fi 2.4 GHz / 5 GHz, Dolby + DTS",
      precio: "369,00 €"
  },
  {
      img: "./img/airpods.jpeg",
      title: "APPLE AIRPODS PRO",
      descripcion: "Airpods - 5,4 g, Caixa - 45,6 g, Caixa de càrrega MagSafe, Bluetooth 5.0, NFC, IPX4, Bateria - 4,5 h",
      precio: "279,56 €"
  }
];



var prod2 = [
  {
      img: "./img/auriculares.jpg",
      title: "Auriculars Rampage Snopy plata",
      descripcion: "Auriculars gaming amb disseny ergonómic preparats per aguantar llarges sessions de joc. Disposen de vibració, llum led i micròfon.",
      precio: "190,14 €"
  },
  {
      img: "./img/router.jpg",
      title: "Router TP-Link Archer AX10 wifi 6",
      descripcion: "Equipat amb tecnologia inalàmbrica Wi-Fi 6 per a velocitats més altres. Arriba a velocitats de fins a 1.5 Gbps (1201 Mbps en 5GHz band i 300 Mbps en 2.4GHz band).",
      precio: "105,99 €"
  },
  {
      img: "./img/discoexterno.jpg",
      title: "SSD Extern SanDisk Extreme Portable SSD 500GB USB 3.2",
      descripcion: "Porta sempre amb tu tota la informació, arxius i documents personals de treball amb el SanDisx Extrem Portable SSD 500 GB USB 2.3. V2 - SSD Extern color negre.",
      precio: "100,79 €"
  },
  {
      img: "./img/portatil.jpg",
      title: "Portàtil MSI Pulse GL76 11UEK-053ES Intel i7-11800H",
      descripcion: "Equipat amb hardware d'última generació, per poder gaudir al màxim. Amb una nova extètica futurista, està equipat amb el súltims processadors Intel Core i7 de 11 Gen 11800H.",
      precio: "1836,67 €"
  },
  {
      img: "./img/adaptadorsonido.jpg",
      title: "Targeta de So Startech Adaptador Sonido USB Externo",
      descripcion: "Targeta de so adaptada d'USB a Audio - Tipos de connector extern: USB 2.0 Tipus A",
      precio: "10,00 €"
  },
  {
      img: "./img/tintaimpresora.jpg",
      title: "HP 304XL Color 300 pàgines",
      descripcion: "Crea documents d'ús diari i fotografies en color d'alta qualitat amb els cartutxos de tita Original HP de baix cost.",
      precio: "29,99 €"
  },
  {
      img: "./img/gt.jpg",
      title: "Videojoc Sony PS4 HITS GT Sport",
      descripcion: "El joc de simulació de conducció exclusiou de PS4 toranrà a estar a les teves mans. Amb una qualitat d'imatge impressionant adaptada a la nova generació, so inmersiu i VR, torna trepitjant fort.",
      precio: "21,99 €"
  },
  {
      img: "./img/impresora.jpg",
      title: "Impresora HP Deskjet 2720e WIFI",
      descripcion: "Té les funcionalitats d'impressió, copia i escàner. Velocitat d'impressió en negre (ISO, A4) de fins a 5,5 ppm. Velocitat d'impressió en color (ISO, A4) de fins a 7,5 ppm.",
      precio: "65,32 €"
  },
  {
      img: "./img/silla.jpg",
      title: "Cadira MSI MAG CH130 I Fabric gris",
      descripcion: "Quan la probis t'encantarà el seu teixit suau i transpirable. Gràcies a la seva silueta ergonómica s'adapta perfectament a la columna vertebral i al cap.",
      precio: "335,89 €"
  },
  {
      img: "./img/raton.jpg",
      title: "Ratolí Logitech Pebble M350 Óptico Bluetooth Eucalipto",
      descripcion: "Ratolí inalàmbric amb tecnologia Bluetooth de disseny minimalista, modern i silenciós. Ratolí compacte que cap a l ateva butxaca i que s'adapta al teu estil de vida.",
      precio: "22,34 €"
  },
  {
      img: "./img/ram.jpg",
      title: "Memoria RAM HyperX Fury RGB DDR4 16GB 3600MHz",
      descripcion: "Ofereix un augment de rendiment i un estil agressio d'inuminació RGB que recorre la lingitud del módul per aconsegurir efectes increïbles i sorprenents.",
      precio: "81,54 €"
  },
  {
      img: "./img/xiaomi.jpg",
      title: "Pulsera d'activitat Xiaomi Mi Smart Band 6 Negra",
      descripcion: "Consulta fàcilment els missatges de text, les trucades i lse notificacions en un moment. Mi Smart Band 6 incorpora una innovadora pantalla gran de vores arrodonides.",
      precio: "37,58 €"
  },
  {
      img: "./img/pc.jpg",
      title: "NOX HUMMER MC USB 3.0 NEGRA",
      descripcion: "Il·luminació LED de 7 colors, HD Audio + USB 2.0-3.0, 2 sistemes de ventilació inclosos, Xassís d'acer SPCC 0.5 mm i Panell frontal de plàstic ABS",
      precio: "59,49 €"
  },
  {
      img: "./img/nintendoswitch.jpg",
      title: "NINTENDO SWITCH – MODELO OLED 7",
      descripcion: "Pantalla HD - (1.280 x 720 px), 4 GB de RAM + 64GB, Pes - 320 g, Bateria - 5,5 h",
      precio: "349,99 €"
  },
  {
      img: "./img/televisor.jpg",
      title: "XIAOMI MI TV 4S 55",
      descripcion: "Smart TV 4K+HDR, 2 GB de RAM + 8 GB, Bluetooth 4.2, Wi-Fi 2.4 GHz / 5 GHz, Dolby + DTS",
      precio: "369,00 €"
  },
  {
      img: "./img/airpods.jpeg",
      title: "APPLE AIRPODS PRO",
      descripcion: "Airpods - 5,4 g, Caixa - 45,6 g, Caixa de càrrega MagSafe, Bluetooth 5.0, NFC, IPX4, Bateria - 4,5 h",
      precio: "279,56 €"
  }
];

var not1 = [
  {
      img: "https://static2.abc.es/media/tecnologia/2021/09/06/internet-2-khK--620x349@abc.jpg",
      etiqueta: "INTERNET",
      fecha: "23/10/2021",
      title: "El 93% de los españoles tienen problemas con el WiFi: ¿cómo pueden mejorar su conexión?"
  },
  {
      img: "https://static2.abc.es/media/tecnologia/2021/09/06/internet-2-khK--620x349@abc.jpg",
      etiqueta: "INTERNET",
      fecha: "23/10/2021",
      title: "El 93% de los españoles tienen problemas con el WiFi: ¿cómo pueden mejorar su conexión?"
  },
  {
      img: "https://static2.abc.es/media/tecnologia/2021/09/06/internet-2-khK--620x349@abc.jpg",
      etiqueta: "INTERNET",
      fecha: "23/10/2021",
      title: "El 93% de los españoles tienen problemas con el WiFi: ¿cómo pueden mejorar su conexión?"
  },
  {
      img: "https://static2.abc.es/media/tecnologia/2021/09/06/internet-2-khK--620x349@abc.jpg",
      etiqueta: "INTERNET",
      fecha: "23/10/2021",
      title: "El 93% de los españoles tienen problemas con el WiFi: ¿cómo pueden mejorar su conexión?"
  },
  {
      img: "https://static2.abc.es/media/tecnologia/2021/09/06/internet-2-khK--620x349@abc.jpg",
      etiqueta: "INTERNET",
      fecha: "23/10/2021",
      title: "El 93% de los españoles tienen problemas con el WiFi: ¿cómo pueden mejorar su conexión?"
  }

];



function afegirProductes(array, id) {
  var sectionProductes = document.getElementById(id);
  var html = "";
  array.forEach(element => {
      html += `<div class="producto-wrap flex flex-column marginTop1em">
                  <div class="producto-img">
                      <img src="${element.img}" alt="" class="width100">
                  </div>
                  <div class="producto-text flex flex-column align-center text-center">
                      <div class="producto-title bold">${element.title}</div>
                      <div class="producto-description">${element.descripcion}</div>
                      <button class="btn">Compra ja</button>
                      <div class="precio-valoracion flex width90 marginTop1em">
                      <div class="producto-valoracion">
                              <form>
                                  <p class="clasificacion">
                                      <input id="radio1" type="radio" name="estrellas" value="5">
                                      <!--
                                          --><label for="radio1">★</label>
                                      <!--
                                          --><input id="radio2" type="radio" name="estrellas" value="4">
                                      <!--
                                          --><label for="radio2">★</label>
                                      <!--
                                          --><input id="radio3" type="radio" name="estrellas" value="3">
                                      <!--
                                          --><label for="radio3">★</label>
                                      <!--
                                          --><input id="radio4" type="radio" name="estrellas" value="2">
                                      <!--
                                          --><label for="radio4">★</label>
                                      <!--
                                          --><input id="radio5" type="radio" name="estrellas" value="1">
                                      <!--
                                          --><label for="radio5">★</label>
                                  </p>
                              </form>
                          </div>
                          <div class="producto-precio bold">${element.precio}</div>
                      </div>
                  </div>
              </div>`;
      
  });
  sectionProductes.innerHTML = html;
}



function afegirNoticies(array, id) {
  var sectionProductes = document.getElementById(id);
  var html = "";
  array.forEach(element => {
      html += `<div class="noticia-wrap">
              <div class="noticia-content">
                  <div class="noticia-img">
                      <img src="${element.img}"
                          alt="" class="width100">
                  </div>
                  <div class="noticia-text-wrap">
                      <div class="noticia-info flex justify-start">
                          <div class="noticias-etiqueta">${element.etiqueta}</div>
                          <div class="">-</div>
                          <div class="noticias-fecha">${element.fecha}</div>
                      </div>
                      <div class="noticia-title">${element.title}</div>
                  </div>
              </div>
          </div>`;
      });
      sectionProductes.innerHTML = html;
  }


afegirProductes(prod2, "productes1");
//afegirNoticies(not1, "noticies1");
//afegirProductes(prod2, "productes2");

